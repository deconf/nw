﻿using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace TTG.NavalWar.NWData
{
	public class Logger
	{
		public static string LogDir = @"d:\bitbucket\NWmanager\";
		public static string LogFilePath = "TTG_NAVALWAR_LOG.log";
		public static string LogIntelFilePath = "intelligence.log";

		protected enum LogLevel
		{
			LevelDebug,
			LevelWarning,
			LevelError
		}

		[DllImport("Kernel32", EntryPoint = "GetCurrentThreadId", ExactSpelling = true)]
		public static extern Int32 GetCurrentWin32ThreadId();

		public static void ClearLogs(string partial = null)
		{
			if (partial != null && !partial.Contains("\n"))
				partial += "\r\n";

			Process process = Process.GetCurrentProcess();

			DirectoryInfo directory = new DirectoryInfo(LogDir);
			foreach (var file in directory.GetFiles("*.log"))
				File.WriteAllText(LogDir + file.Name, partial ?? $"[pid:{process.Id}/tid:{GetCurrentWin32ThreadId()}]\r\n");
		}

		protected void Log(LogLevel logLevel, string message)
		{
			try
			{
				if (!message.Contains("\r\n"))
					message += "\r\n";

				StreamWriter sw = File.AppendText(LogDir + LogFilePath);
				sw.Write($"<{GetCurrentWin32ThreadId()}>" + LogLevelName(logLevel) + "#"
					+ DateTime.Now.ToLongTimeString() + "." + DateTime.Now.Millisecond.ToString()
					+ " " + message);
				sw.Close();
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
		}

		public static void dump(string prefix, object obj, string fileName = null)
		{
			if (fileName == null)
				fileName = "debug";

			var jsonSettings = new JsonSerializerSettings
			{
				ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
				Formatting = Formatting.Indented,
				NullValueHandling = NullValueHandling.Ignore,
				MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
				MaxDepth = 2

			};

			string json = string.Empty;

			try
			{
				json = JsonConvert.SerializeObject(obj, jsonSettings);
			}
			catch (Exception e)
			{
				exception(e);
			}

			if (json.Length > 0)
				log($"{prefix}: {json}\r\n", fileName);
		}

		public static void exception(Exception e)
		{
			log($"exception in {e.TargetSite.Name}@{e.Source} [{e.Message}]\r\nstack:\r\n{e.StackTrace}\r\n#\r\n" +
				$"{(e.InnerException?.StackTrace)}", "exception");
		}

		public static void log(string message, string fileName = null)
		{
			try
			{
				if (!message.Contains("\r\n"))
					message += "\r\n";

				StreamWriter sw = File.AppendText(LogDir + (fileName != null ? fileName + ".log" : LogIntelFilePath));
				sw.Write($"[{GetCurrentWin32ThreadId()}#" + DateTime.Now.ToLongTimeString() + "." + DateTime.Now.Millisecond.ToString()
					+ "] " + message);
				sw.Close();

				GamePlay.Payloads.JammerLog(message);
			}
			catch (Exception e)
			{
			}
		}

		public void LogDebug(string message)
		{
			Log(LogLevel.LevelDebug, message);
		}

		public void LogWarning(string message)
		{
			Log(LogLevel.LevelWarning, message);
		}

		public void LogError(string message)
		{
			Log(LogLevel.LevelError, message);
		}


		private string LogLevelName(LogLevel logLevel)
		{
			switch (logLevel)
			{
				case LogLevel.LevelDebug:
					return "Debug";
				//break;
				case LogLevel.LevelWarning:
					return "Warning";
				//break;
				case LogLevel.LevelError:
					return "Error";
				//break;
				default:
					return "Unknown";
					//break;
			}
		}

		public static string var_dump(object obj, int recursion = 0)
		{
			StringBuilder result = new StringBuilder();

			// Protect the method against endless recursion
			if (recursion < 5)
			{
				// Determine object type
				Type t = obj.GetType();

				// Get array with properties for this object
				PropertyInfo[] properties = t.GetProperties();

				foreach (PropertyInfo property in properties)
				{
					try
					{
						// Get the property value
						object value = property.GetValue(obj, null);

						// Create indenting string to put in front of properties of a deeper level
						// We'll need this when we display the property name and value
						string indent = String.Empty;
						string spaces = "|   ";
						string trail = "|...";

						if (recursion > 0)
						{
							indent = new StringBuilder(trail).Insert(0, spaces, recursion - 1).ToString();
						}

						if (value != null)
						{
							// If the value is a string, add quotation marks
							string displayValue = value.ToString();
							if (value is string) displayValue = String.Concat('"', displayValue, '"');

							// Add property name and value to return string
							result.AppendFormat("{0}{1} = {2}\n", indent, property.Name, displayValue);

							try
							{
								if (!(value is ICollection))
								{
									// Call var_dump() again to list child properties
									// This throws an exception if the current property value
									// is of an unsupported type (eg. it has not properties)
									result.Append(var_dump(value, recursion + 1));
								}
								else
								{
									// 2009-07-29: added support for collections
									// The value is a collection (eg. it's an arraylist or generic list)
									// so loop through its elements and dump their properties
									int elementCount = 0;
									foreach (object element in ((ICollection) value))
									{
										string elementName = String.Format("{0}[{1}]", property.Name, elementCount);
										indent = new StringBuilder(trail).Insert(0, spaces, recursion).ToString();

										// Display the collection element name and type
										result.AppendFormat("{0}{1} = {2}\n", indent, elementName, element.ToString());

										// Display the child properties
										result.Append(var_dump(element, recursion + 2));
										elementCount++;
									}

									result.Append(var_dump(value, recursion + 1));
								}
							}
							catch { }
						}
						else
						{
							// Add empty (null) property to return string
							result.AppendFormat("{0}{1} = {2}\n", indent, property.Name, "null");
						}
					}
					catch
					{
						// Some properties will throw an exception on property.GetValue()
						// I don't know exactly why this happens, so for now i will ignore them...
					}
				}
			}

			log($"dump: {result.ToString()}");
			return result.ToString();
		}
	}
}
