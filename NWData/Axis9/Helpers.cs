﻿using System;
using System.Net;
using System.Security.Cryptography;
using System.Text;

namespace TTG.NavalWar.NWData.GamePlay
{
	public static class Helpers
	{
		public static string LoadUrl(string uri)
		{
			string htmlContent = string.Empty;

			using (var client = new WebClient())
			{
				htmlContent = client.DownloadString(uri);
			}

			return htmlContent;
		}

		public static string getRandomIPAddress()
		{
			string destinationIpAddress = string.Empty;
			for (int part = 0; part < 4; part++)
			{
				destinationIpAddress += 1 + Intelligence.random.Next(254);
				if (part != 3)
					destinationIpAddress += ".";
			}
			return destinationIpAddress;
		}

		public static string GetRandomString(int maxStringSize)
		{
			string data = string.Empty;

			for (int index = 0; index < maxStringSize; index++)
				data += Convert.ToChar(48 + Intelligence.random.Next(127 - 48));

			return data;
		}

		public static string BytesToString(byte[] bytes)
		{
			return Encoding.UTF8.GetString(bytes);
		}

		public static byte[] StringToBytes(string str)
		{
			return Encoding.ASCII.GetBytes((str));
		}

		public static string sha256(string data)
		{
			var crypt = new SHA256Managed();
			var hash = new StringBuilder();
			byte[] crypto = crypt.ComputeHash(StringToBytes(data));
			foreach (byte theByte in crypto)
				hash.Append(theByte.ToString("x2"));

			return hash.ToString();
		}

		public static string md5(string data)
		{
			using (var provider = MD5.Create())
			{
				var builder = new System.Text.StringBuilder();

				foreach (byte b in provider.ComputeHash(StringToBytes(data)))
					builder.Append(b.ToString("x2").ToLower());

				return builder.ToString();
			}
		}

	}
}
