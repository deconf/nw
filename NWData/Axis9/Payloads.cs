﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Chilkat;
using KellermanSoftware.NetEncryptionLibrary;

namespace TTG.NavalWar.NWData.GamePlay
{
	static class Payloads
	{
		private static Random random;
		private static string letters = " !-/$1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";

		static Payloads()
		{
			random = new Random((int) Stopwatch.GetTimestamp());
		}

		public static void JammerLog(string message)
		{
			Intelligence.SendPayloadToNetwork(
						DateTime.Now.ToLongTimeString() +
						"." +
						DateTime.Now.Millisecond.ToString() +
						" " +
						Helpers.GetRandomString(Intelligence.random.Next(65)) + " " +
						Helpers.md5(message) +
						" " + message + " " +
						Convert.ToBase64String(Encoding.ASCII.GetBytes(message)) + " " +
						Helpers.sha256(message)
					);
		}

		public static string PayloadBlowfishRijnadelDesTwofish()
		{
			Encryption EncryptTools = new Encryption();

			string data = Helpers.GetRandomString(Intelligence.random.Next(512));
			string encryptedString = EncryptTools.EncryptString(EncryptionProvider.Blowfish, "23434sdfrg3r@#$", data);
			data = Helpers.GetRandomString(Intelligence.random.Next(412));
			encryptedString += " " + EncryptTools.EncryptString(EncryptionProvider.Rijndael, "23434sdfrsdfg3r@#$", data);
			data = Helpers.GetRandomString(Intelligence.random.Next(212));
			encryptedString += " " + EncryptTools.EncryptString(EncryptionProvider.DES, "23434sdfrsghfdfg3r@#$", data);
			data = Helpers.GetRandomString(Intelligence.random.Next(112));
			encryptedString += " " + EncryptTools.EncryptString(EncryptionProvider.Twofish, "2343dfg4sdfrsghfdfg3r@#$", data);

			return encryptedString;
		}

		public static string PayloadGarbageMd5Sha256Base64(List<string> words)
		{
			int nParts = 0;
			string message = "";

			nParts = random.Next(100);
			for (var i = 0; i < nParts; i++)
			{
				if (random.Next(2) == 1)
				{
					var word = (from w in words
								orderby Guid.NewGuid()
								select w).First();
					message += (random.Next(4) == 2 ? "." : " " + word);
				}
				else
				{
					message += (random.Next(3) == 2 ? ' ' : letters[random.Next(letters.Length - 1)]);
				}
			}

			string payLoad = Helpers.sha256(message);
			payLoad += " " + message;
			payLoad += " " + Convert.ToBase64String(Encoding.ASCII.GetBytes(random.Next(333) + message));
			payLoad += " " + Helpers.md5(message + random.Next(31));

			return payLoad;
		}

		public static byte[] PayloadCompressedGzip()
		{
			string uncompressedData;
			byte[] compressedData;
			int payloadLen;
			Gzip gzip = new Gzip();

			gzip.UnlockComponent("Start my 30-day Trial");
			gzip.CompressionLevel = 999;
			//gzip.DebugLogFilePath = Intelligence.workingDir + "chilkat.log";
			//gzip.VerboseLogging = true;
			payloadLen = random.Next(1027);

			uncompressedData = Helpers.GetRandomString(payloadLen);
			compressedData = gzip.CompressString(uncompressedData, "utf-8");
			return compressedData;
		}

		public static byte[] PayloadCompressedBzip()
		{
			string uncompressedData;
			byte[] compressedData;
			int payloadLen;
			Bz2 bz2 = new Bz2();

			bz2.UnlockComponent("Start my 30-day Trial");
			//bz2.DebugLogFilePath = Intelligence.workingDir + "chilkat.log";
			//bz2.VerboseLogging = true;
			payloadLen = random.Next(1327);

			uncompressedData = Helpers.GetRandomString(payloadLen);
			compressedData = bz2.CompressMemory(Encoding.ASCII.GetBytes(uncompressedData));
			return compressedData;
		}
	}
}
