﻿using System;
using System.IO;
using System.Reflection;
using System.Diagnostics;

namespace TTG.NavalWar.NWComms
{
	public class Logger
	{
		public static string LogDir = @"d:\bitbucket\nwmanager\";
		public static string filePath = LogDir + FILE_NAME + ".log";
		private const string FILE_NAME = "TTG_NAVALWAR_LOG";

		protected enum LogLevel
		{
			LevelDebug,
			LevelWarning,
			LevelError
		}

		protected void Log(LogLevel logLevel, string message)
		{

			try
			{
				StreamWriter sw = File.AppendText(filePath);
				sw.WriteLine(LogLevelName(logLevel) + " - "
					+ DateTime.Now.ToLongTimeString() + "." + DateTime.Now.Millisecond.ToString()
					+ " " + message);
				sw.Close();
			}
			catch (Exception)
			{

				//ignore
			}
		}

		public static void exception(Exception e)
		{
			log($"exception in {e.TargetSite.Name}@{e.Source} [{e.Message}]\r\nstack:\r\n{e.StackTrace}\r\n",
				"exception");
		}

		public static void log(string message, string fileName =
			 @"d:\bitbucket\nwmanager\TTG_NAVALWAR_LOG")
		{
			string LogDir = @"d:\bitbucket\nwmanager\";
			string filePath = LogDir + "intelligence.log";
			try
			{
				StreamWriter sw = File.AppendText(fileName);
				sw.WriteLine(DateTime.Now.ToLongTimeString() + "." + DateTime.Now.Millisecond.ToString() + ": " + message);
				sw.Close();
			}
			catch (Exception)
			{

				//ignore
			}
		}

		[Conditional("LOG_DEBUG")]
		public void LogDebug(string message)
		{
			Log(LogLevel.LevelDebug, message);
		}

		[Conditional("LOG_WARNING")]
		public void LogWarning(string message)
		{
			Log(LogLevel.LevelWarning, message);
		}

		[Conditional("LOG_ERROR")]
		public void LogError(string message)
		{
			Log(LogLevel.LevelError, message);
		}

		private string LogLevelName(LogLevel logLevel)
		{
			switch (logLevel)
			{
				case LogLevel.LevelDebug:
					return "Debug";
				//break;
				case LogLevel.LevelWarning:
					return "Warning";
				//break;
				case LogLevel.LevelError:
					return "Error";
				//break;
				default:
					return "Unknown";
					//break;
			}
		}
	}
}
